## Intent
To efficently share announcements, initiatives, and feedback within the Ops Sub-dept management team we are experimenting with an async meeting process driven by this issue.

## Tasks

### Opening Tasks
- [ ] Retro Thread - @sgoldstein

### Meeting Tasks
- [ ] Add relevant items to agenda - @sgoldstein
- [ ] Post slack update in #doe-ops - @sgoldstein
    - [ ] Item headlines
    - [ ] Request that others contribute discussion items

### Closing Tasks
- [ ] Review and respond to async discussion in agenda doc - @sgoldstein

/assign @sgoldstein

/due Friday