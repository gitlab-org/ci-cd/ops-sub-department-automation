# GitLab Ops Sub-Department Automation
* [Handbook](https://about.gitlab.com/handbook/engineering/development/ops/)

This repository also includes code to automate the scheduled creation of issues for the Ops Sub-Department.  It is forked from [https://gitlab.com/gitlab-com/Product](https://gitlab.com/gitlab-com/Product). With the goal of extracting the functionality to a common library which could be used in both places.

## Usage

Update [issues.yml](https://gitlab.com/gitlab-org/ci-cd/ops-sub-department-automation/-/blob/main/config/issues.yml) and [section.yml](https://gitlab.com/gitlab-org/ci-cd/ops-sub-department-automation/-/blob/main/config/section.yml) with the appropriate config:
* **Issue Title** = Issue.name + (optionally Section.name)
* **Issue Description** = Will parse template in `/templates` (for `section` scoped) or `/.gitlab/issue_templates` (for `all` scoped) directory and render as markdown
* **Date created** = Will create on the daily run that corresponds with the Issue.day_of_month or Issue.day_of_week (in [three letter abbreviated form](https://ruby-doc.org/stdlib-2.6.1/libdoc/date/rdoc/DateTime.html#method-i-strftime))
* **Issue.scope** determines at what level to create issues, either `all` or `section`. See limitations below

## Testing Locally
To test the create_issues script use the `--dry-run` arguement. Before running, make sure you set the `CI_PROJECT_ID` (for the product project, it's 1641463) and the `GITLAB_API_PRIVATE_TOKEN` environment variables.

```
bundle exec ruby create_issues.rb --dry-run
```

## Testing YAML Syntax

YAML can be hard. You can check the YAML file syntax locally with these commands:

First install YAMLlint
```
gem install yamllint
```

And once installed testing the two primary yaml files
```
yamllint config/issues.yml config/sections.yml
```

## Limitations

* The only scopes that are available are `section` and `all`. We should consider adding `group` that would allow for the creation of an issue per product group. 
* In order to use multiple quick actions you have to add an extra line space between them
