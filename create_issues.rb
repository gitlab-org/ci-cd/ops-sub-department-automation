require 'gitlab'
require 'yaml'
require 'erb'
require 'date'


dry_run = false
today = Date.today
project_id = ENV['CI_PROJECT_ID']
api_token = ENV['GITLAB_API_PRIVATE_TOKEN']

class IssueCreator
    def initialize(today)
        @today = today
    end
    def run(api_token, project_id)
        connect(api_token)
        puts 'Loading content from config/issues.yml'
        @issues = YAML.load_file('config/issues.yml')
        puts 'Loading content from config/sections.yml'
        @sections = YAML.load_file('config/sections.yml')
        puts 'Content loaded successfully.'
        puts 'Walking issues to create...'
        @issues['issues'].each do |issue|
            if issue['day_of_month']
                ## Using Day of Month Value
                puts '* ' << issue['name'] << ' should be created on the ' << issue['day_of_month'].to_s << ' day of the month.'
                today_date = @today.strftime("%-d")
                puts '** Today is the ' << today_date
                if today_date == issue['day_of_month'].to_s
                    create(issue['name'], issue['template'], issue['scope'], "Monthly", project_id)
                end
            end
            if issue['day_of_week']
                ## Using Day of week value
                puts '* ' << issue['name'] << ' should be created every ' << issue['day_of_week'].to_s << '.'
                today_day = @today.strftime("%a")
                puts '** Today is ' << today_day
                if today_day == issue['day_of_week'].to_s
                    create(issue['name'], issue['template'], issue['scope'], "Weekly", project_id)
                end
            end
        end
        puts 'Run Complete.'
    end
    
    def connect(api_token)
        puts "Connecting to GitLab..."
        @gitlab = Gitlab.client(
            endpoint: 'https://gitlab.com/api/v4',
            private_token: api_token
        )
        @user = @gitlab.user
        puts "Connection successful. Connected user email: " << @user.email
    end
    def week_of_label(date)
        (date + date.wday + 1).strftime("%a, %Y-%m-%d")
    end

    def create(name, templateName, scope, frequency, project_id)
        frequencyText = ""
        case frequency
        when "Monthly"
            frequencyText = Date::MONTHNAMES[@today.month] 
        when "Weekly"
            frequencyText = "Week of" + week_of_label(@today)
        end
        if scope == 'all'
            ## implies we don't need to use an ERB template just regular issue templates
            template = File.open('.gitlab/issue_templates/' << templateName).read
            puts '** Template loaded...'
            description = ERB.new(template,0,'>').result(binding)
            puts '** Description generated...'
            
            @gitlab.create_issue(project_id, name + ' : ' + frequencyText ,{description: description, assignee_id: @user.id})
            puts  '** ' << name << ' issue created.'
        end
        if scope == 'section'
            @sections['sections'].each do |section|
                ## Adjust this to create the description based on template and section variables
                template = File.open('templates/' << templateName << '.erb').read
                puts '** Template loaded...'
                description = ERB.new(template,0,'>').result(binding)
                description << "\n\n \n\n 🤖 Beep bop. Upgrades accepted [here](https://gitlab.com/gitlab-com/Product/-/blob/main/create_issues.rb)!"
                puts '** Description generated...'
                @gitlab.create_issue(project_id, name + ' ' + section['name'] + ': ' + frequencyText ,{description: description, assignee_id: @user.id})
                puts  '** ' << name << ' issue created.'
            end
        end
    end
end

# Mock GitLab user and service
class UserMock
    def initialize
        @email = "mockuser@gitlab.com"
        @id = 1234
    end
    attr_accessor :email, :id
end
class GitLabMock
    def initialize
        @user = UserMock.new()
    end
    def create_issue(project, title, options)
        puts "Would have created issue:"
        puts "  Project: " + project
        puts "  Title:   " + title
        puts "  Content: \n" + options[:description] + "\n\n"
    end
    attr_accessor :user
end
# Override issue creator to use GitLab mock
class IssueCreatorTest < IssueCreator
    def connect(api_token)
        puts "Connecting to GitLab..."
        @gitlab = GitLabMock.new()
        @user = @gitlab.user
        puts "Connection successful. Connected user email: " << @user.email
    end
end

if $PROGRAM_NAME == __FILE__
    # Override with mock if we are doing a dry run
    for arg in ARGV
        if arg == "--dry-run"
            dry_run = true
            api_token = "mocktoken"
            project_id = "mockproject"            
            for d in 1..31
                # Simulate every day of the month
                today = Date::strptime(d.to_s + "-01-2020","%d-%m-%Y")
                issue_creator = IssueCreatorTest.new(today)
                issue_creator.run(api_token, project_id)
            end
        end
    end
    if !dry_run
        issue_creator = IssueCreator.new(today)
        issue_creator.run(api_token, project_id)
    end
end
